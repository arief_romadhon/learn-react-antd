import "./App.css";
import Login from "./components/login/login";
import { Switch, Route } from "react-router-dom";
import PageRegistration from "./components/pages/pageRegistration";
import Dashboard from "./components/dashboard/dashboard";
import PageLogin from "./components/pages/pageLogin";

function App() {
  return (
    <>
      <Dashboard />
      {/* <Switch>
        <Route path="/register" component={PageRegistration} />
        <Route path="/login" component={PageLogin} />
      </Switch> */}
    </>
  );
}

export default App;
