import React from "react";
import RegistrationForm from "../register/register";

const PageRegistration = () => {
  return (
    <>
      <RegistrationForm />
    </>
  );
};

export default PageRegistration;
