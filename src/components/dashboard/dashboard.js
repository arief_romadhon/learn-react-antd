import React from "react";
import { Layout, Menu } from "antd";
import { Switch, Route } from "react-router-dom";
import PageLogin from "../pages/pageLogin";
import PageRegistration from "../pages/pageRegistration";

import {
  AppstoreOutlined,
  BarChartOutlined,
  CloudOutlined,
  ShopOutlined,
  TeamOutlined,
  UserOutlined,
  UploadOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";

const { Header, Content, Footer, Sider } = Layout;

const Dashboard = () => {
  return (
    <Layout>
      <Sider
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
        }}
      >
        <div className="logo" />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["4"]}>
          <Menu.Item key="1" icon={<VideoCameraOutlined />}>
            <Link to="/login">Login</Link>
          </Menu.Item>
          <Menu.Item key="2" icon={<UserOutlined />}>
            <Link to="/register">Register</Link>
          </Menu.Item>
          <Menu.Item key="3" icon={<UserOutlined />}>
            <Link to="/search-player">Search Player</Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout
        className="site-layout"
        style={{ marginLeft: 200, height: "100vh" }}
      >
        <Header className="site-layout-background" style={{ padding: 0 }} />
        <Content style={{ margin: "24px 16px 0", overflow: "initial" }}>
          <Switch>
            <Route path="/register" component={PageRegistration} />
            <Route path="/login" component={PageLogin} />
          </Switch>
        </Content>
        <Footer
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <div>
            <p>Ant Design ©2021 Created by Ant UED</p>
          </div>
        </Footer>
      </Layout>
    </Layout>
  );
};

export default Dashboard;
